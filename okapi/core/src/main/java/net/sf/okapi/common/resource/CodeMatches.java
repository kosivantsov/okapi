/*===========================================================================
  Copyright (C) 2021 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.common.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

final public class CodeMatches {
	public static int NO_MATCH = Integer.MIN_VALUE;
	public static int ADDED_MATCH = Integer.MAX_VALUE;
	public static CodeMatches NO_CODES = new CodeMatches(0, 0);
	public static CodeMatches SAME_CODES = new CodeMatches(0, 0);

	private final int[] fromMatches;
	private final int[] toMatches;
	private final boolean[] toIsolated;
	private final boolean[] fromIsolated;

	private CodeMatches(int fromSize, int toSize) {
		fromMatches = new int[fromSize];
		toMatches = new int[toSize];
		toIsolated = new boolean[toSize];
		fromIsolated = new boolean[fromSize];

		Arrays.fill(fromMatches, NO_MATCH);
		Arrays.fill(toMatches, NO_MATCH);
		Arrays.fill(toIsolated, false);
		Arrays.fill(fromIsolated, false);
	}

	public CodeMatches(TextFragment from, TextFragment to) {
		this(from.codes.size(), to.codes.size());

		// set isolated flag
		for (int i = 0; i < from.length(); i++) {
			switch (from.charAt(i)) {
				case TextFragment.MARKER_ISOLATED:
					int index = TextFragment.toIndex(from.charAt(i + 1));
					fromIsolated[index] = true;
			}
		}

		for (int i = 0; i < to.length(); i++) {
			switch (to.charAt(i)) {
				case TextFragment.MARKER_ISOLATED:
					int index = TextFragment.toIndex(to.charAt(i + 1));
					toIsolated[index] = true;
			}
		}
	}

	public int[] getFromMatches() {
		return fromMatches;
	}

	public int[] getToMatches() {
		return toMatches;
	}

	public void setFromMatch(int index, int id) {
		fromMatches[index] = id;
	}

	public void setToMatch(int index, int id) {
		toMatches[index] = id;
	}

	public int getToMatchIndex(int toIndex) {
		if (isToMatch(toIndex)) {
			return toMatches[toIndex];
		}
		return NO_MATCH;
	}

	public boolean isToMatch(int index) {
		return toMatches[index] != NO_MATCH;
	}

	public boolean isToIsolated(int index) {
		return toIsolated[index];
	}

	public boolean isFromIsolated(int index) {
		return fromIsolated[index];
	}

	public boolean hasFromMismatch() {
		for (int m : fromMatches) {
			if (m == NO_MATCH) {
				return true;
			}
		}
		return false;
	}

	public boolean hasToMismatch() {
		for (int m : toMatches) {
			if (m == NO_MATCH) {
				return true;
			}
		}
		return false;
	}

	public int getFromMismatchCount() {
		int count = 0;
		for (int m : fromMatches) {
			if (m == NO_MATCH) {
				count++;
			}
		}
		return count;
	}

	public int getToMismatchCount() {
		int count = 0;
		for (int m : toMatches) {
			if (m == NO_MATCH) {
				count++;
			}
		}
		return count;
	}

	public Iterable<Integer> getFromMismatchIterator() {
		return getMismatchIterator(fromMatches);
	}

	public Iterable<Integer> getToMismatchIterator() {
		return getMismatchIterator(toMatches);
	}

	private Iterable<Integer> getMismatchIterator(int[] matches) {
		List<Integer> mismatchIndexes = new ArrayList<>();
		int i = -1;
		for (int m : matches) {
			i++;
			if (m == NO_MATCH) {
				mismatchIndexes.add(i);
			}
		}
		return mismatchIndexes;
	}
}
