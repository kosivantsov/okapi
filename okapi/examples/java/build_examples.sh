echo "==== Building example01 ===="
rm -fr example01/target
mkdir example01/target
javac -d example01/target example01/src/main/java/*.java -cp .:../lib/*
jar cf example01/target/okapi-example-01.jar -C example01/target .

echo "==== Building example02 ===="
rm -fr example02/target
mkdir example02/target
javac -d example02/target example02/src/main/java/*.java -cp .:../lib/*
jar cf example02/target/okapi-example-02.jar -C example02/target .

echo "==== Building example03 ===="
rm -fr example03/target
mkdir example03/target
cp example03/src/main/resources/* example03/target
javac -d example03/target example03/src/main/java/*.java -cp .:../lib/*
jar cf example03/target/okapi-example-03.jar -C example03/target .

echo "==== Building example04 ===="
rm -fr example04/target
mkdir example04/target
javac -d example04/target example04/src/main/java/*.java -cp .:../lib/*
jar cf example04/target/okapi-example-04.jar -C example04/target .

echo "==== Building example05 ===="
rm -fr example05/target
mkdir example05/target
javac -d example05/target example05/src/main/java/*.java -cp .:../lib/*
jar cf example05/target/okapi-example-05.jar -C example05/target .

echo "==== Building example06 ===="
rm -fr example06/target
mkdir example06/target
javac -d example06/target example06/src/main/java/*.java -cp .:../lib/*
jar cf example06/target/okapi-example-06.jar -C example06/target .

echo "==== Building example07 ===="
rm -fr example07/target
mkdir example07/target
cp example07/src/main/resources/* example07/target
javac -d example07/target example07/src/main/java/*.java -cp .:../lib/*
jar cf example07/target/okapi-example-07.jar -C example07/target .
