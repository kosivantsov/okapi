package net.sf.okapi.roundtrip.integration;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.integration.EventRoundTripIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xliff2.XLIFF2Filter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class RoundTripXliff2IT extends EventRoundTripIT {
	private static final String CONFIG_ID = "okf_xliff2";
	private static final String DIR_NAME = "/xliff2/";
	private static final List<String> EXTENSIONS = Arrays.asList(".xliff", ".xlf", "xlf2");

	final static FileLocation root = FileLocation.fromClass(RoundTripXliff2IT.class);

	public RoundTripXliff2IT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS);
	}

	@Before
	public void setUp() throws Exception {
		filter = new XLIFF2Filter();
	}

	@After
	public void tearDown() throws Exception {
		filter.close();
	}

	@Test
	public void debug() throws FileNotFoundException, URISyntaxException {
		final File file = root.in("/xliff2/update_target.xlf").asFile();
		runTest(true, file, null, null, new FileComparator.EventComparatorWithWhitespace());
	}

	@Test
	public void deepenXliff2() throws FileNotFoundException, URISyntaxException {
		setExtensions(Arrays.asList(".deepen_xlf"));
		realTestFiles(true, new FileComparator.EventComparatorIgnoreSegmentation());
	}

	@Test
	public void xliff2Files() throws FileNotFoundException, URISyntaxException {		
		realTestFiles(true, new FileComparator.EventComparatorWithWhitespace());
	}
}
